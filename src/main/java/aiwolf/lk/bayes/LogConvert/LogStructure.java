package aiwolf.lk.bayes.LogConvert;

import java.util.Arrays;

public class LogStructure {

    int day = 0;
    String type = "";
    String[] attribute;

    public LogStructure(String[] data) {
        this.day = Integer.parseInt(data[0]);
        this.type = data[1];
        this.attribute = Arrays.copyOfRange(data, 2, data.length);
    }
}
