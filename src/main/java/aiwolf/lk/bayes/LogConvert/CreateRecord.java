package aiwolf.lk.bayes.LogConvert;

import aiwolf.lk.bayes.LogConvert.board.*;
import aiwolf.lk.bayes.db.DBConnector;
import fukurou.logger.Logger;
import org.aiwolf.common.data.*;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * 確率変数の作成を担うクラス
 * 並列処理クラス
 */
public class CreateRecord implements Callable<Record> {

    private BoardSurface boardSurface;
    private String tableName;
    private Agent submit;
    private Agent target;
    private int day;

    static class Builder {
        private BoardSurface boardSurface;
        private String tableName;
        private Agent submit;
        private Agent target;
        private int day = -1;

        Builder logPick(LogPick logPick) {
            this.boardSurface = new LogPickBoard(logPick);
            return this;
        }

//        public Builder gameInfo(GameInfo gameInfo) {
//            this.boardSurface = new GameInfoBoard(gameInfo);
//            return this;
//        }

        Builder tableName(String tableName) {
            this.tableName = tableName;
            return this;
        }

        Builder submit(Agent submit) {
            this.submit = submit;
            return this;
        }

        Builder target(Agent target) {
            this.target = target;
            return this;
        }

        Builder day(int day) {
            this.day = day;
            return this;
        }

        CreateRecord build() {
            if (boardSurface == null || tableName == null || submit == null || target == null || day == -1) {
                throw new NullPointerException("すべてのフィールドに値がセットされないまま，レコードを生成することはできません");
            }
            return new CreateRecord(this);
        }
    }
    private CreateRecord(Builder builder) {
        this.boardSurface = builder.boardSurface;
        this.tableName = builder.tableName;
        this.submit = builder.submit;
        this.target = builder.target;
        this.day = builder.day;
    }

    @Override
    public Record call() {
        // 列名を取得
        List<String> columnNameList = DBConnector.columnNameOfTable(tableName);
        // 列名に従って要素を取得
        Map<String, String> record = new HashMap<>();
        for (String columnName :
                columnNameList) {
            record.put(columnName, extractValue(columnName));
        }

        Record recordInstance = new Record();
        // 確率変数を格納
        recordInstance.setRecordMap(record);
        // 生存人数を格納
        recordInstance.setNumAliveAgents(boardSurface.numAliveAgents(this.day));
        return recordInstance;
    }

    /**
     * 列名に応じて観測値を返却する
     * @param columnName 列名
     * @return 観測値
     */
    private String extractValue(String columnName) {
        // 列名を_でパース
        List<String> columnNameParseList = new ArrayList<>(Arrays.asList(columnName.split("_")));
        // 列名の先頭から順に分けて処理
        String value = "";
        switch (columnNameParseList.get(0)) {
            case "status":
                value = statusValue(columnNameParseList);
                break;
            case "talk":
                value = talkValue(columnNameParseList);
                break;
            case "decision":
                value = decisionValue(columnNameParseList);
                break;
            default:
                Logger.warn("不正な列名です．columnName: " + columnName);
        }
        return value;
    }

    /**
     * decisionで始まる列名の値を返す
     * @param columnNameParseList 列名をパースしたリスト
     * @return 列名に対応する値
     */
    private String decisionValue(List<String> columnNameParseList) {
        switch (columnNameParseList.get(1)) {
            case "vote":
                switch (columnNameParseList.get(2)) {
                    case "target":
                        return decisionValueVote(columnNameParseList, target);
                    case "submit":
                        return decisionValueVote(columnNameParseList, submit);
                }
                break;
        }

        return "";
    }

    /**
     * decision_voteで始まる列名の値を返す
     * @param columnNameParseList 列名をパースしたリスト
     * @param voteAgent 誰の投票（投票者）を扱うか
     * @return 列名に対応する値
     */
    private String decisionValueVote(List<String> columnNameParseList, Agent voteAgent) {
        LinkedHashSet<Vote> voteSet = boardSurface.voteSetAll(day, voteAgent);
        switch (columnNameParseList.get(3)) {
            case "CO":
                switch (columnNameParseList.get(4)) {
                    case "VILLAGER":
                        return countTargetCORole(voteSet, Role.VILLAGER);
                    case "SEER":
                        return countTargetCORole(voteSet, Role.SEER);
                    case "BODYGUARD":
                        return countTargetCORole(voteSet, Role.BODYGUARD);
                    case "MEDIUM":
                        return countTargetCORole(voteSet, Role.MEDIUM);
                    case "POSSESSED":
                        return countTargetCORole(voteSet, Role.POSSESSED);
                    case "WEREWOLF":
                        return countTargetCORole(voteSet, Role.WEREWOLF);
                }
                break;
            case "alive":
                switch (columnNameParseList.get(4)) {
                    case "ALIVE":
                        return countTargetStatus(voteSet, AgentStatus.ALIVE);
                    case "EXECUTED":
                        return countTargetStatus(voteSet, AgentStatus.EXECUTED);
                    case "ATTACKED":
                        return countTargetStatus(voteSet, AgentStatus.ATTACKED);
                }
                break;
            case "knownSpecies":
                switch (columnNameParseList.get(4)) {
                    case "HUMAN":
                      return countTargetKnownSpecies(voteSet, KnownSpecies.HUMAN);
                    case "WEREWOLF":
                        return countTargetKnownSpecies(voteSet, KnownSpecies.WEREWOLF);
                    case "UNKNOWN":
                        return countTargetKnownSpecies(voteSet, KnownSpecies.UNKNOWN);
                }
                break;
            case "grayScale":
                switch (columnNameParseList.get(4)) {
                    case "WHITE":
                        return countTargetGrayScale(voteSet, GrayScale.WHITE);
                    case "BLACK":
                        return countTargetGrayScale(voteSet, GrayScale.BLACK);
                    case "GRAY":
                        return countTargetGrayScale(voteSet, GrayScale.GRAY);
                    case "MADARA":
                        return countTargetGrayScale(voteSet, GrayScale.MADARA);
                }
                break;
            case "AgentS":
                return (int)voteSet.stream().filter(vote -> vote.getTarget().equals(submit)).count() + "";
            case "AgentT":
                return (int)voteSet.stream().filter(vote -> vote.getTarget().equals(target)).count() + "";
        }
        return "";
    }

    /**
     * talkで始まる列名の値を返す
     * @param columnNameParseList 列名をパースしたリスト
     * @return 列名に対応する値
     */
    private String talkValue(List<String> columnNameParseList) {
        return "";
    }

    /**
     * statusで始まる列名の値を返す
     * @param columnNameParseList 列名をパースしたリスト
     * @return 列名に対応する値
     */
    private String statusValue(List<String> columnNameParseList) {
        switch(columnNameParseList.get(1)) {
            case "divinedResultAll":
                Set<Judge> divinedResultAll = boardSurface.divinedResultAll(day);
                if (columnNameParseList.get(2).equals(AgentAlias.AgentT.toString())) { // status_divinedResult_AgentT
                    Optional<String> result = divinedResultAll.stream().filter(judge -> judge.getTarget().equals(target)).map(judge -> judge.getResult().toString()).findFirst();
                    return result.orElse("NON");

                } else if (columnNameParseList.get(2).equals(AgentAlias.AgentO.toString())){
                    List<String> resultAgentO = divinedResultAll.stream().filter(judge -> !judge.getTarget().equals(target) && !judge.getTarget().equals(submit) && boardSurface.isAlive(day, judge.getTarget())).map(judge -> judge.getResult().toString()).collect(Collectors.toList());
                    if (columnNameParseList.get(3).equals(Species.HUMAN.toString())) {  // status_divinedResult_AgentO_HUMAN
                        int count = (int)resultAgentO.stream().filter(s -> s.equals(Species.HUMAN.toString())).count();
                        if (count > 5) {
                            count = 5;
                        }
                        return count + "";
                    } else if (columnNameParseList.get(3).equals(Species.WEREWOLF.toString())) {    // status_divinedResult_AgentO_WEREWOLF
                        int count = (int)resultAgentO.stream().filter(s -> s.equals(Species.WEREWOLF.toString())).count();
                        if (count > 5) {
                            count = 5;
                        }
                        return count + "";
                    }
                }
            case "guardSuccess":
                HashMap<Agent, Boolean> guardSuccessMap = boardSurface.guardSuccessMap(day);
                if (columnNameParseList.get(2).equals(AgentAlias.AgentT.toString())) {  // status_guardSuccess_AgentT
                    return guardSuccessMap.entrySet().stream()
                            .filter(keySet -> keySet.getKey().equals(target))
                            .anyMatch(Map.Entry::getValue) + "";
                } else if (columnNameParseList.get(2).equals(AgentAlias.AgentO.toString())) {   // status_guardSuccess_AgentO
                    int count = (int)guardSuccessMap.entrySet().stream()
                            .filter(Map.Entry::getValue)
                            .count();
                    return count + "";
                }
            case "medium":
                if (columnNameParseList.get(2).equals(AgentAlias.AgentO.toString())) {
                    if (columnNameParseList.get(3).equals(Species.HUMAN.toString())) {  // status_medium_AgentO_HUMAN
                        return (int)boardSurface.mediumResultList(day).stream().filter(judge -> judge.getResult().equals(Species.HUMAN)).count() + "";
                    } else if (columnNameParseList.get(3).equals((Species.WEREWOLF.toString()))) {  // status_medium_AgentO_WEREWOLF
                        return (int)boardSurface.mediumResultList(day).stream().filter(judge -> judge.getResult().equals(Species.WEREWOLF)).count() + "";
                    }
                }
            case "collaborator":
                if (columnNameParseList.get(2).equals(AgentAlias.AgentT.toString())) {  // status_collaborator_AgentT
                    return boardSurface.knownSpecies(submit, day, target).equals(KnownSpecies.WEREWOLF) + "";
                }

        }
        return "";
    }

    /**
     * 対象者が投票した投票先のグレースケール
     * @param voteSet 対象者の投票情報すべて
     * @param grayScale カウントしたいグレースケール
     * @return 投票数
     */
    private String countTargetGrayScale(LinkedHashSet<Vote> voteSet, GrayScale grayScale) {
        AtomicInteger count = new AtomicInteger();
        for (Vote vote : voteSet) {
            if (boardSurface.receivingGrayScale(day, vote.getTarget()).equals(grayScale)) {
                count.getAndIncrement();
            }
        }
        return count + "";
    }

    /**
     * 対象者が投票した投票先のわかっている種族
     * @param voteSet 対象者の投票情報全て
     * @param knownSpecies カウントしたい種族
     * @return 投票数
     */
    private String countTargetKnownSpecies(LinkedHashSet<Vote> voteSet, KnownSpecies knownSpecies) {
        AtomicInteger count = new AtomicInteger();
        for (Vote vote : voteSet) {
            if (boardSurface.knownSpecies(submit, day, vote.getTarget()).equals(knownSpecies)) count.getAndIncrement();
        }
        if (count.get() > 5) {
            count.set(5);
        }
        return count +"";
    }

    /**
     * 対象者が投票した投票先の状態（生存、追放、被襲撃）（最新役職）
     * @param voteSet 対象者の投票情報すべて
     * @return 投票数
     */
    private String countTargetStatus(LinkedHashSet<Vote> voteSet, AgentStatus agentStatus) {
        AtomicInteger count = new AtomicInteger();
        for (Vote vote : voteSet) {
            if (boardSurface.agentStatus(day, vote.getTarget()).equals(agentStatus)) count.getAndIncrement();
        }
        if (count.get() > 5) {
            count.set(5);
        }
        return count + "";
    }

    /**
     * 対象者が投票した投票先の役職（最新役職）
     * @param voteSet その日の投票情報
     * @return 投票数
     */
    private String countTargetCORole(LinkedHashSet<Vote> voteSet, Role role) {
        AtomicInteger count = new AtomicInteger();
        for (Vote vote :
                voteSet) {
            if (boardSurface.latestCORole(day, vote.getTarget()).equals(role)) count.getAndIncrement();
        }
        if (count.get() > 5) {
            count.set(5);
        }
        return count + "";
    }

}
