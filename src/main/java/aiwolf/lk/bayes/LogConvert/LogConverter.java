package aiwolf.lk.bayes.LogConvert;

import aiwolf.lk.bayes.util.PropertyUtil;
import fukurou.logger.LogLevel;
import fukurou.logger.Logger;
import org.aiwolf.common.data.Agent;
import org.aiwolf.common.data.Role;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

/**
 * ゲームのログファイルを確率変数に変換するクラス
 *
 * 確率変数はデータベースの列名に準拠し、作成される
 */
public class LogConverter {

    private static String serverLogFilePath;
    private static Role createRole;
    private static int numParticipateAgents;
    private static int numReadLogs;

    static {
        serverLogFilePath = PropertyUtil.getProperty("serverLogFilePath");
        if (serverLogFilePath.equals("")) {
            Logger.fatal("プロパティ値が不正です。" +  serverLogFilePath);
        }
        try {
            createRole = Role.valueOf(PropertyUtil.getProperty("role"));
            numParticipateAgents = Integer.parseInt(PropertyUtil.getProperty("numParticipateAgents"));
            numReadLogs = Integer.parseInt(PropertyUtil.getProperty("numReadLogs", "1", LogLevel.TRACE));
        } catch (NumberFormatException e) {
            Logger.fatal("Need set numParticipateAgets or numReadLogs." + numParticipateAgents + ", " + numReadLogs);
        } catch (IllegalArgumentException e) {
            Logger.fatal("Check role name." + PropertyUtil.getProperty("role"));
        }
    }

    /**
     * ログファイルから確率変数へ変換
     * @return 確率変数
     */
    public List<Record> convert() {
        // ログファイルのリストを取得
        String[] serverLogFiles = logFileList();

        // エージェントリストの作成
        List<Agent> agentList = new ArrayList<>();
        for (int agentNumber = 1; agentNumber <= numParticipateAgents; agentNumber++) {
            agentList.add(Agent.getAgent(agentNumber));
        }

        // 並列処理準備
        ExecutorService executorService = Executors.newWorkStealingPool();
        List<Future<Record>> futureList = new ArrayList<>();

        // 変換処理
        for (int i = 0; i < numReadLogs; i++) {
            assert serverLogFiles != null;
            LogPick logPick = new LogPick(new File(serverLogFiles[i]));

            // submitループ
            for (Agent submit : agentList) {
                if (!logPick.assignRole(submit).equals(createRole)) continue;   // 取得する役職でない場合はログ取得しない
                // targetループ
                for (Agent target : agentList) {
                    // 日にちループ
                    for (int day = 0; day <= logPick.endDay(); day++) {
                        if (submit.equals(target) || !logPick.isAlive(day, submit) || !logPick.isAlive(day, target)) continue;  // submitとtargetが同一自分であるかを確認する、また、それぞれの生存確認

                        futureList.add(executorService.submit(new CreateRecord.Builder()
                                .logPick(logPick)
                                .tableName(createRole + "_" + numParticipateAgents + "_" + logPick.numAliveAgents(day))
                                .submit(submit)
                                .target(target)
                                .day(day)
                                .build()
                        ));
                    }
                }
            }
        }

        List<Record> recordList = new ArrayList<>();
        // 進行状況を出力
        for (int i = 0; i < futureList.size(); i++) {
            try {
                recordList.add(futureList.get(i).get());

                if (Logger.getPrintLogLevel().equals(LogLevel.INFO)) {
                    System.out.print(String.format("\r[%3.2f / 100]", ((i / (double) futureList.size()) * 100)));
                }
            } catch (InterruptedException | ExecutionException e) {
                Logger.error("An Exception occurred during processing.");
            }
        }

        executorService.shutdown();

        // 全てのスレッドが終了するまで待機（3日）
        try {
            if (!executorService.awaitTermination(3, TimeUnit.DAYS)) {
                Logger.warn("Processing did not finish.");
                executorService.shutdown();
                if (!executorService.awaitTermination(1, TimeUnit.MINUTES)) {
                    Logger.fatal("Processing did not finish...");
                }
            }
        } catch (InterruptedException e) {
            Logger.warn("Occurred an unexpected interrupt during processing.");
        }

        Logger.info("Transfer of data from log file to DB is complete.");
        return recordList;
    }

    private String[] logFileList() {
        String[] serverLogFiles = new File(serverLogFilePath).list();
        // ログファイルのリストが取得されていない場合はfatalを出し、強制終了
        if (serverLogFiles != null && serverLogFiles.length != 0) {
            // プロパティファイルに記載されている読み込むログファイル数よりディレクトリ内にあるログファイル数が少ないとき、読み込むログファイル数をディレクトリ内にあるログファイル数へ変更
            if (serverLogFiles.length < numReadLogs) {
                Logger.warn("The setting value is too large. The value is adjusted automatically. " + serverLogFiles.length);
                numReadLogs = serverLogFiles.length;
            }
            serverLogFiles = Arrays.copyOfRange(serverLogFiles, 0, numReadLogs);
        } else {
            Logger.fatal("Server Logs not found. serverLogFilePath: " + serverLogFilePath + " serverLogFiles: " + Arrays.toString(serverLogFiles));
        }
        return serverLogFiles;
    }
}
