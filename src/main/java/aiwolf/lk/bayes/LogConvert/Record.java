package aiwolf.lk.bayes.LogConvert;

import java.util.HashMap;
import java.util.Map;

/**
 * ログファイルから確率変数に変換する際に用いる構造体
 */
public class Record {
    // 確率変数
    private Map<String, String> recordMap = new HashMap<>();
    // 生存人数
    private int numAliveAgents = 0;

    public synchronized Map<String, String> getRecordMap() {
        return recordMap;
    }

    public synchronized void setRecordMap(Map<String, String> recordMap) {
        this.recordMap = recordMap;
    }

    public int getNumAliveAgents() {
        return numAliveAgents;
    }

    public void setNumAliveAgents(int numAliveAgents) {
        this.numAliveAgents = numAliveAgents;
    }
}
