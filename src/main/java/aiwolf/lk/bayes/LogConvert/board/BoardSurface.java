package aiwolf.lk.bayes.LogConvert.board;

import org.aiwolf.common.data.*;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * GameInfoとLogPickの差を埋めるためのクラス
 */
public abstract class BoardSurface {

    public abstract Set<Judge> divinedResultAll(int day); // その日までのすべての占い結果を返す．（過去に取得した結果を累積していくため，2日目を参照した後，1日目までに取得した占い結果のみを取得することはできない）
    public abstract boolean isAlive(int day, Agent agent); // 対象エージェントが生存しているかを返す
    public abstract LinkedHashSet<Vote> voteSetAll(int day, Agent agent);        // その日までのすべてのagentの投票したvoteを返す． （過去に取得した結果を累積していくため，2日目を参照した後，1日目までに取得した占い結果のみを取得することはできない）
    public abstract Role latestCORole(int day, Agent agent);  // その日までにエージェントがCOした最新の役職
    public abstract AgentStatus agentStatus(int day, Agent agent);   // 対象エージェントのステータス（ALIVE, EXECUTED, ATTACKED)を返す
    public abstract KnownSpecies knownSpecies(Agent submit, int day, Agent target);  // その日までに対象エージェントの判明している（システム上）種族
    public abstract GrayScale receivingGrayScale(int day, Agent agent);   // その日までに対象エージェントが受けているグレースケール（自分自身の発言も含む）
    public abstract int numAliveAgents(int day);       // 生存者数
    public abstract HashMap<Agent, Boolean> guardSuccessMap(int day);   // その日までにガードした人と成功判定のマップ
    public abstract List<Judge> mediumResultList (int day); // その日までに霊能した結果のリスト
    public abstract Agent nextDivinedTarget();    // その日に占う対象
    public abstract Agent nextAttackTarget();     // その日に襲撃する対象
    public abstract Agent nextExecuteTarget();    // その日に追放する対象
    public abstract Team winTeam();               // 勝利したチーム
    public abstract int endDay();             // ゲーム終了日
    public abstract List<Vote> preVoteList();     // 前日のvoteリスト
//    public abstract TalkListManagement talkListManagement();
    public abstract Agent submit();           // 投票者
    public abstract Agent target();           // 投票先候補者
    public abstract Judge preDivinedResult(); // nullable
    public abstract Judge divinedResult();    // nullable
    public abstract Role assignRole();        // nullable
    public abstract int day();

    protected GrayScale checkGrayScale(List<Species> receivingDivinationResultList) {
        GrayScale tmpReceivingDivinationResult = null;
        for (Species receivingDivinationResult :
                receivingDivinationResultList) {
            if (tmpReceivingDivinationResult == null) {
                if (receivingDivinationResult.equals(Species.HUMAN)) {
                    tmpReceivingDivinationResult = GrayScale.WHITE;
                } else if (receivingDivinationResult.equals(Species.WEREWOLF)) {
                    tmpReceivingDivinationResult = GrayScale.BLACK;
                }
            } else if (tmpReceivingDivinationResult.equals(GrayScale.WHITE) && receivingDivinationResult.equals(Species.WEREWOLF)) {
                tmpReceivingDivinationResult = GrayScale.MADARA;
            } else if (tmpReceivingDivinationResult.equals(GrayScale.BLACK) && receivingDivinationResult.equals(Species.HUMAN)) {
                tmpReceivingDivinationResult = GrayScale.MADARA;
            }
        }
        return tmpReceivingDivinationResult;
    }


}
