package aiwolf.lk.bayes.LogConvert.board;

public enum AgentStatus {
    ALIVE,
    EXECUTED,
    ATTACKED
}
