package aiwolf.lk.bayes.LogConvert.board;

import fukurou.logger.Logger;
import org.aiwolf.client.lib.Content;
import org.aiwolf.client.lib.Topic;
import org.aiwolf.common.data.*;
import org.aiwolf.common.net.GameInfo;

import java.util.*;
import java.util.stream.Collectors;

public class GameInfoBoard extends BoardSurface {

    private GameInfo gameInfo;

    private Set<Judge> divinedResultAll = new HashSet<>();
    private LinkedHashSet<Vote> voteSetAll = new LinkedHashSet<>();
    private LinkedHashSet<Agent> attackedAgentList = new LinkedHashSet<>();
    private LinkedHashSet<Agent> executedAgentList = new LinkedHashSet<>();
    private LinkedHashMap<Agent, Boolean> guardedAgentSuccessMap = new LinkedHashMap<>();
    private List<Judge> mediumResultList = new ArrayList<Judge>();

    public GameInfoBoard(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    @Override
    public Set<Judge> divinedResultAll(int day) {
        divinedResultAll.add(gameInfo.getDivineResult());
        return divinedResultAll;
    }

    @Override
    public boolean isAlive(int day, Agent agent) {
        return gameInfo.getAliveAgentList().contains(agent);
    }

    @Override
    public LinkedHashSet<Vote> voteSetAll(int day, Agent agent) {
        List<Vote> voteList = gameInfo.getVoteList();
        if (voteList != null) {
            for (Vote vote :
                    voteList) {
                if (vote.getAgent().equals(agent)) {
                    voteSetAll.add(vote);
                }
            }
        }
        return voteSetAll;
    }

    @Override
    public Role latestCORole(int day, Agent agent) {
        List<Talk> agentCOTalkList = gameInfo.getTalkList().stream()
                .filter(talk ->
                    talk.getAgent().equals(agent)
                            && new Content(talk.getText()).getTopic().equals(Topic.COMINGOUT)
                )
                .collect(Collectors.toList());
        if (!agentCOTalkList.isEmpty()) {
            Talk talk = agentCOTalkList.get(agentCOTalkList.size()-1);
            return new Content(talk.getText()).getRole();
        }
        return null;
    }

    @Override
    public AgentStatus agentStatus(int day, Agent agent) {
        if (gameInfo.getAliveAgentList().contains(agent)) return AgentStatus.ALIVE;
        attackedAgentList.add(gameInfo.getAttackedAgent());    // latestAttackedAgentのほうがいいのかも
        executedAgentList.add(gameInfo.getExecutedAgent());
        if (attackedAgentList.contains(agent)) return AgentStatus.ATTACKED;
        if (executedAgentList.contains(agent)) return AgentStatus.EXECUTED;
        Logger.error("Unable to get agent status. Returned 'ALIVE' status");
        return AgentStatus.ALIVE;
    }

    @Override
    public KnownSpecies knownSpecies(Agent submit, int day, Agent target) {
        if (submit.equals(target)) {    // 自分自身
            if (gameInfo.getRole().equals(Role.WEREWOLF)) return KnownSpecies.WEREWOLF;
            return KnownSpecies.HUMAN;
        }
        if (gameInfo.getRole().equals(Role.SEER)) {
            if (divinedResultAll.stream()
                    .filter(judge -> judge.getDay() <= day
                            && judge.getTarget().equals(target))
                    .anyMatch(judge -> judge.getResult().equals(Species.HUMAN)))    // 占い結果
                return KnownSpecies.HUMAN;
            if (divinedResultAll.stream()
                    .filter(judge -> judge.getDay() <= day
                            && judge.getTarget().equals(target))
                    .anyMatch(judge -> judge.getResult().equals(Species.WEREWOLF))) // 占い結果
                return KnownSpecies.WEREWOLF;
        } else if (gameInfo.getRole().equals(Role.BODYGUARD)) {
            if (gameInfo.getAttackedAgent() == null && gameInfo.getGuardedAgent().equals(target)) return KnownSpecies.HUMAN;    // 護衛成功
        } else if (gameInfo.getRole().equals(Role.WEREWOLF)) {
            List<Agent> werewolves = gameInfo.getRoleMap().entrySet().stream()
                    .filter(agentRoleEntry -> agentRoleEntry.getValue().equals(Role.WEREWOLF))
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            if (werewolves.contains(target)) return KnownSpecies.WEREWOLF;  // 人狼仲間
        }
        if (agentStatus(day, target).equals(AgentStatus.ATTACKED)) return KnownSpecies.HUMAN;   // 被襲撃者
        return KnownSpecies.UNKNOWN;
    }

    @Override
    public GrayScale receivingGrayScale(int day, Agent agent) {
        TalkListManager talkListManager = new TalkListManager(gameInfo.getTalkList().stream()
                .filter(talk ->
                        talk.getDay() <= day)
                .collect(Collectors.toList()));
        // 占い師、霊能者、狩人、狂人COが一人だけの場合は白
        Role agentComingoutRole = talkListManager.latestComingoutRole(agent);
        if (agentComingoutRole != null && !agentComingoutRole.equals(Role.WEREWOLF) && !agentComingoutRole.equals(Role.VILLAGER) && talkListManager.numComingout(agentComingoutRole) ==1) {
            return GrayScale.WHITE;
        }
        // 占い師から白判定を一つだけ受けている場合は白
        // 占い師から黒判定を1つだけ受けている場合は黒
        // 占い師から白判定と黒判定を受けている場合は斑
        List<Species> receivingDivinationResultList = talkListManager.receivingDivinationResult(agent);
        if (receivingDivinationResultList.size() > 0) {
            return checkGrayScale(receivingDivinationResultList);
        }

        // agentが襲撃されている場合は白
        if (agentStatus(day, agent).equals(AgentStatus.ATTACKED)) {
            return GrayScale.WHITE;
        }

        // agentが追放されており、霊能者から判定を受けている場合は白or黒
        List<Species> receivingIndicateResultList = talkListManager.receivingIdentifiedResult(agent);
        if (agentStatus(day, agent).equals(AgentStatus.EXECUTED) && receivingIndicateResultList.size() > 0) {
            return checkGrayScale(receivingIndicateResultList);
        }

        return GrayScale.GRAY;
    }

    @Override
    public int numAliveAgents(int day) {
        return gameInfo.getAliveAgentList().size();
    }

    @Override
    public HashMap<Agent, Boolean> guardSuccessMap(int day) {
        Agent guardedAgent = gameInfo.getGuardedAgent();
        Agent attackedAgent = gameInfo.getAttackedAgent();

        if (guardedAgent != null && attackedAgent != null) {
            guardedAgentSuccessMap.put(guardedAgent, guardedAgent.equals(attackedAgent));
        }

        return guardedAgentSuccessMap;
    }

    @Override
    public List<Judge> mediumResultList (int day) {
        Judge mediumResult = gameInfo.getMediumResult();
        if (mediumResult != null) {
            mediumResultList.add(mediumResult);
        }
        return mediumResultList;
    }


    @Override
    public Agent nextDivinedTarget() {
        return null;
    }

    @Override
    public Agent nextAttackTarget() {
        return null;
    }

    @Override
    public Agent nextExecuteTarget() {
        return null;
    }

    @Override
    public Team winTeam() {
        return null;
    }

    @Override
    public int endDay() {
        return 0;
    }

    @Override
    public List<Vote> preVoteList() {
        return null;
    }

    @Override
    public Agent submit() {
        return null;
    }

    @Override
    public Agent target() {
        return null;
    }

    @Override
    public Judge preDivinedResult() {
        return null;
    }

    @Override
    public Judge divinedResult() {
        return null;
    }

    @Override
    public Role assignRole() {
        return null;
    }

    @Override
    public int day() {
        return 0;
    }
}
