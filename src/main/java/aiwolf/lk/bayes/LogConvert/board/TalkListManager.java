package aiwolf.lk.bayes.LogConvert.board;

import org.aiwolf.client.lib.Content;
import org.aiwolf.client.lib.Topic;
import org.aiwolf.common.data.Agent;
import org.aiwolf.common.data.Role;
import org.aiwolf.common.data.Species;
import org.aiwolf.common.data.Talk;

import java.util.List;
import java.util.stream.Collectors;


public class TalkListManager {

    private List<Talk> talkList;

    public TalkListManager(List<Talk> talkList) {
        this.talkList = talkList;
    }

    /**
     * Agentが受けている霊能結果をリストにして返す
     * @param agent しらべるAgent
     * @return 占い結果のリスト
     */
    public List<Species> receivingIdentifiedResult(Agent agent) {
        return talkList.stream()
                .filter(talk ->  {
                    Content content = new Content(talk.getText());
                    if (content.getTopic().equals(Topic.IDENTIFIED) && content.getTarget().equals(agent)) {
                        return true;
                    }
                    return false;
                })
                .map(talk -> new Content(talk.getText()).getResult())
                .collect(Collectors.toList());
    }
    /**
     * Agentが受けている占い結果をリストにして返す
     * @param agent 調べるAgent
     * @return 占い結果のリスト
     */
    public List<Species> receivingDivinationResult(Agent agent) {
        return talkList.stream()
                .filter(talk -> {
                    Content content = new Content(talk.getText());
                    if (content.getTopic().equals(Topic.DIVINED) && content.getTarget().equals(agent)) {
                        return true;
                    }
                    return false;
                })
                .map(talk -> new Content(talk.getText()).getResult())
                .collect(Collectors.toList());
    }
    /**
     * Agentが最後にCOMINGOUTした役職を返す
     * @param agent 調べるエージェント
     * @return 最後にCOMINGOUTした役職
     */
    public Role latestComingoutRole (Agent agent) {
        List<Talk> comingoutTalkList = talkList.stream()
                .filter(talk -> {
                    Content content = new Content(talk.getText());
                    if (content.getTopic().equals(Topic.COMINGOUT) && content.getSubject().equals(agent)) return true;
                    return false;
                })
                .collect(Collectors.toList());
        if (comingoutTalkList.size() > 0) {
            return new Content(comingoutTalkList.get(comingoutTalkList.size()-1).getText()).getRole();
        }
        return null;
    }

    /**
     * ある役職をCOMINGOUTしている数を返却
     *
     * @param role カウントする役職
     * @return 数
     */
    public int numComingout(Role role) {
        return (int)talkList.stream()
                .filter(talk -> {
                    Content content = new Content(talk.getText());
                    if (content.getTopic().equals(Topic.COMINGOUT) && content.getRole().equals(role)) return true;
                    return false;
                })
                .count();
    }

}
