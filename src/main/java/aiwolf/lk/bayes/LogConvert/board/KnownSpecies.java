package aiwolf.lk.bayes.LogConvert.board;

public enum KnownSpecies {
    HUMAN,
    WEREWOLF,
    UNKNOWN
}
