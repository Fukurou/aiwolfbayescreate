package aiwolf.lk.bayes.LogConvert;

import fukurou.logger.Logger;
import org.aiwolf.common.data.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LogPick {

    private List<LogStructure> logStructureList = new ArrayList<>();

    public LogPick(File logFile) {
        // csvからList<LogStructure>へ
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(logFile));

            String line;
            while((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(",", 0);
                LogStructure logStructure = new LogStructure(data);
                logStructureList.add(logStructure);
            }
//            Logger.trace("Load file is " + logFile);
            bufferedReader.close();
        } catch (IOException e) {
            Logger.error("ログファイルの読み込みに失敗しました．logFile: " + logFile);
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * agentに割り当てられた役職を取得する
     * @param agent 参照するagent
     * @return agentに割り当てられた役職名
     */
    public Role assignRole(Agent agent) {
        List<String> myRoles = logStructureList.stream()
                .filter(logStructure -> logStructure.day == 0
                        && logStructure.type.equals("status")
                        && Integer.parseInt(logStructure.attribute[0]) == (agent.getAgentIdx()))
                .map(logStructure -> logStructure.attribute[1])
                .collect(Collectors.toList());
        if (!myRoles.isEmpty()) {
            Role role = Role.valueOf(myRoles.get(0));
            Logger.trace(String.format("%sに割り当てられた役職: %s", agent, role));
            return role;
        }
        Logger.warn("不完全なログ，または，不正な引数が渡されました．agent: " + agent);
        return null;
    }

    /**
     * 占い結果を返す
     * @param day 占い結果を取得する日時（1日目の占い師が取得する占い結果を取得する場合，0を指定する
     * @return 占い結果
     */
    public Judge divinedResult(int day) {
        List<LogStructure> divine = logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("divine"))
                .collect(Collectors.toList());

        if (!divine.isEmpty()) {
            LogStructure oneDivine = divine.get(0);
            Agent submit = Agent.getAgent(Integer.parseInt(oneDivine.attribute[0]));
            Agent target = Agent.getAgent(Integer.parseInt(oneDivine.attribute[1]));
            Species species = Species.valueOf(oneDivine.attribute[2]);
//            Logger.trace(String.format("day: %s, 占い師: %s, 占い対象: %s, 占い結果: %s", day, submit, target, species));
            return new Judge(day, submit, target, species);
        }
        Logger.trace("占われたエージェントは発見されません．day: " + day);    // 1日目に占い師が追放されるとログにdivineログが存在しない
        return null;
    }

    /**
     * 発言リストを返す(talkList)
     * @return 発言リスト
     */
    public List<Talk> talkList() {
        return logStructureList.stream()
                .filter(logStructure -> logStructure.type.equals("talk"))
                .map(logStructure -> {
                    int idx = Integer.parseInt(logStructure.attribute[0]);
                    int day = logStructure.day;
                    int turn = Integer.parseInt(logStructure.attribute[1]);
                    Agent agent = Agent.getAgent(Integer.parseInt(logStructure.attribute[2]));
                    String text = logStructure.attribute[3];
                    return new Talk(idx, day, turn, agent, text);
                })
                .collect(Collectors.toList());
    }

    /**
     * 襲撃されたエージェントを返す
     * 襲撃投票ではなく，実際に襲撃されたエージェントが返却される
     * @param day 襲撃された日時
     * @return 襲撃されたエージェント
     */
    public Agent attackedAgent (int day) {
        List<Agent> attackAgentList = logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("attack"))
                .map(logStructure -> Agent.getAgent(Integer.parseInt(logStructure.attribute[0])))
                .collect(Collectors.toList());
        if (!attackAgentList.isEmpty()) {
            Agent agent = attackAgentList.get(0);
            Logger.trace("襲撃されたエージェント: " + agent);
            return agent;
        }
//        Logger.trace("襲撃されたエージェントは発見されません．");    // 1日目に人狼が追放されるとログにattackログが存在しない
        return null;
    }

    /**
     * 襲撃されたエージェントとその成否判定を返す
     * 襲撃投票ではなく、実際に襲撃され当たエージェントが返却される
     * @param day
     * @return
     */
    public Map<Agent, Boolean> attackedAgentResult(int day) {
        List<LogStructure> tmpLogStructureList = logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("attack"))
                .collect(Collectors.toList());

        Map<Agent, Boolean> agentBooleanMap = new HashMap<>();
        for (LogStructure logStructure :
                tmpLogStructureList) {
            agentBooleanMap.put(Agent.getAgent(Integer.parseInt(logStructure.attribute[0])), Boolean.valueOf(logStructure.attribute[1]));
        }

        return agentBooleanMap;
    }

    /**
     * 追放されたエージェントを返す
     * @param day 追放された日時
     * @return 追放されたエージェント
     */
    public Agent executedAgent(int day) {
        List<Agent> executedAgentList = logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("execute"))
                .map(logStructure -> Agent.getAgent(Integer.parseInt(logStructure.attribute[0])))
                .collect(Collectors.toList());
        if (!executedAgentList.isEmpty()) {
            Agent agent = executedAgentList.get(0);
            Logger.trace("追放されたエージェント: " + agent);
            return agent;
        }
        Logger.warn("不完全なログ，または不正な引数が渡されました．day: " + day);
        return null;


    }

    /**
     * 投票者と投票先を保管するVoteのリストを返す
     * @param day 取得する投票リストの日時
     * @return その日のVoteリスト
     */
    public List<Vote> voteList (int day) {
        List<LogStructure> voteLogList = logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("vote"))
                .collect(Collectors.toList());

        List<Agent> votedAgent = new ArrayList<>(); // 再投票された場合，最初に投票した結果のみを取得する
        List<Vote> voteList = new ArrayList<>();
        for (LogStructure logStructure :
                voteLogList) {
            Agent submit = Agent.getAgent(Integer.parseInt(logStructure.attribute[0]));
            Agent target = Agent.getAgent(Integer.parseInt(logStructure.attribute[1]));
            if (!votedAgent.contains(submit)) {
                voteList.add(new Vote(day, submit, target));
                votedAgent.add(submit);
            }
        }
        if (!voteList.isEmpty()) {
//            Logger.trace("投票リスト: " + voteList);
            return voteList;
        }
        Logger.warn("不完全なログ，または不正な引数が渡されました．day: " + day);
        return null;
    }

    /**
     * 勝利した陣営を返す
     * @return 勝利した陣営
     */
    public Team winTeam() {
        List<LogStructure> resultLogList = logStructureList.stream()
                .filter(logStructure -> logStructure.type.equals("result"))
                .collect(Collectors.toList());
        if (!resultLogList.isEmpty()) {
            Team winTeam = Team.valueOf(resultLogList.get(0).attribute[2]);
            Logger.trace("勝利した陣営: " + winTeam);
            return winTeam;
        }
        Logger.warn("不完全なログのため，勝利した陣営を取得できません．");
        return null;
    }

    /**
     * ゲームが終了する日時を返す．
     * 1日目でゲームが終了する場合，1を．2日目でゲームが終了する場合2を返却する
     * @return ゲームが終了する日時
     */
    public int endDay() {
        List<LogStructure> resultLogList = logStructureList.stream()
                .filter(logStructure -> logStructure.type.equals("result"))
                .collect(Collectors.toList());
        if (!resultLogList.isEmpty()) {
            int endDay = resultLogList.get(0).day - 1;
            Logger.trace("ゲーム終了日: " + endDay);
            return endDay;
        }
        Logger.warn("不完全なログのため，終了日を取得できません．");
        return 0;
    }

    /**
     * エージェントが生存しているか
     * @param day 生存を確認する日時
     * @param agent 生存を確認するエージェント
     * @return 生存している=true
     */
    public boolean isAlive(int day, Agent agent) {
        List<String> aliveList = logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("status")
                        && logStructure.attribute[0].equals(agent.getAgentIdx() + ""))
                .map(logStructure -> logStructure.attribute[2])
                .collect(Collectors.toList());
        if (!aliveList.isEmpty()) {
            boolean isAlive = aliveList.get(0).equals("ALIVE");
            Logger.trace(String.format("%sの生存: %s", agent, isAlive));
            return isAlive;
        }
        Logger.warn(String.format("不完全なログ，または不正な引数が渡されました．int: %s Agent %s", day, agent));
        return true;
    }

    /**
     * エージェントが生存している数を返す
     * @param day 生存を確認する日時
     * @return 生存数
     */
    public int numAliveAgents(int day) {
        return (int)logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("status")
                        && logStructure.attribute[2].equals("ALIVE"))
                .count();
    }

    /**
     * 参加者数を返す
     * @return 参加者数
     */
    public int numAgents() {
        return (int)logStructureList.stream()
                .filter(logStructure -> logStructure.day == 0
                        && logStructure.type.equals("status"))
                .count();
    }

    /**
     * 護衛対象を返す
     * @param day 護衛した日
     * @return 護衛した対象エージェントが存在しない場合はnullを返す（狩人が死亡した日を含め、狩人がいない場合、ログから取得できない）
     */
    public Agent guardedAgent(int day) {
        List<String> agentNumberList = logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("guard"))
                .map(logStructure -> logStructure.attribute[1])
                .collect(Collectors.toList());
        if (agentNumberList.size() == 1) {
            return Agent.getAgent(Integer.parseInt(agentNumberList.get(0)));
        }
        if (agentNumberList.size() < 1) {
            Logger.warn("Occurred unknown error. Need to check the log file." + agentNumberList.size());
        }
        return null;
    }

    /**
     * 各エージェントに割り当てられた役職をMapにして返却
     * @return 各エージェントに割り当てられた役職
     */
    public Map<Agent, Role> getRoleMap() {
        List<LogStructure> statusList = logStructureList.stream()
                .filter(logStructure -> logStructure.day == 0
                        && logStructure.type.equals("status"))
                .collect(Collectors.toList());
        Map<Agent, Role> roleMap = new HashMap<>();
        for (LogStructure status :
                statusList) {
            roleMap.put(Agent.getAgent(Integer.parseInt(status.attribute[0])), Role.valueOf(status.attribute[1]));
        }
        return roleMap;
    }

    /**
     * 霊能結果を返す
     * @param day 霊能結果を得た日
     * @return 霊能結果
     */
    public Judge getMediumResult(int day) {
        List<Judge> list  = logStructureList.stream()
                .filter(logStructure -> logStructure.day == day
                        && logStructure.type.equals("execute"))
                .map(logStructure -> new Judge(
                        day,
                        Agent.getAgent(0),
                        Agent.getAgent(Integer.parseInt(logStructure.attribute[0])),
                        Role.valueOf(logStructure.attribute[1]).getSpecies()
                ))
                .collect(Collectors.toList());
        if (!list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
}
