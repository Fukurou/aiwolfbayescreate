package aiwolf.lk.bayes.util;

import fukurou.logger.LogLevel;
import fukurou.logger.Logger;

import javax.imageio.IIOException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertyUtil {

    private final static String PROPERTY_FILE_PATH = "resource/common.properties";
    private final static Properties properties = new Properties();

    static {
        try {
            properties.load(Files.newBufferedReader(Paths.get(PROPERTY_FILE_PATH), StandardCharsets.UTF_8));
        } catch (IOException e) {
            Logger.fatal("Faild Property files");
        }
    }
    public static String getProperty(final String key) {
        return getProperty(key, "");
    }

    public static String getProperty(final String key, final String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    /**
     * プロパティ値を取得する。
     *
     * デフォルト値が返却される場合には、指定されたログレベルに合わせてログが出力される。
     * @param key　取得キー
     * @param defaultValue　キーがない場合のデフォルト値
     * @param logLevel　ログ出力レベル
     * @return　プロパティ値
     */
    public static String getProperty(final String key, final String defaultValue, LogLevel logLevel) {
        String property = properties.getProperty(key);
        if (property == null) {
            Logger.submit(logLevel, "set default value is " + key);
            return defaultValue;
        }
        return property;
    }


}
