package aiwolf.lk.bayes.main;

import aiwolf.lk.bayes.LogConvert.LogConverter;
import aiwolf.lk.bayes.LogConvert.Record;
import aiwolf.lk.bayes.db.DBConnector;

import aiwolf.lk.bayes.util.PropertyUtil;
import fukurou.logger.Logger;
import org.aiwolf.common.data.Role;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Create database in DB
 *
 * Create database from file that was written schema of database.
 * After that, move data to database from game log file.
 */
class DatabaseCreation {

    private static String tableDataDirectoryPath;
    private static Role createRole;
    private static int numParticipateAgents;

    static {
        tableDataDirectoryPath = PropertyUtil.getProperty("tableDataFilePath");
        if (tableDataDirectoryPath.equals("")) {
            Logger.fatal("Invalid property value" + tableDataDirectoryPath);
        }
        try {
            createRole = Role.valueOf(PropertyUtil.getProperty("role"));
            numParticipateAgents = Integer.parseInt(PropertyUtil.getProperty("numParticipateAgents"));
        } catch (NumberFormatException e) {
            Logger.fatal("Need set numParticipateAgets or numReadLogs." + numParticipateAgents);
        } catch (IllegalArgumentException e) {
            Logger.fatal("Check role name." + PropertyUtil.getProperty("role"));
        }
    }

    /**
     * DB作成
     */
    static void create() {
        // 作成するテーブル名のプレフィックス
        String createPrefixTableName =  createRole + "_" + numParticipateAgents;
        // DBにテーブルを作成する
        Logger.info("Create Table...");
        int numCreateTable = createTable(new File(tableDataDirectoryPath).list(), createPrefixTableName);
        Logger.debug("Number of create table is " + numCreateTable);

        // 確率変数に変換
        Logger.info("Convert probability variable...");
        List<Record> recordList = new LogConverter().convert();

        // DBへ確率変数を登録していく
        Logger.info("Insert probability variable to Database");
        for (Record record :
                recordList) {
            String tableName = createPrefixTableName + "_" + record.getNumAliveAgents();
            DBConnector.executeUpdate(tableName, record.getRecordMap());
        }

    }

    /*
     * DBにテーブルを作成する
     * @param tableDataFiles　テーブルデータがあるファイルのリスト
     * @param createRole　作成する役職
     * @return 作成されたテーブル数
     */
    private static int createTable(String[] tableDataFiles, String createPrefixTableName) {
        int numCreateTable = 0;
        if (tableDataFiles != null) {
            List<String> tableDataFileList = Arrays.stream(tableDataFiles)
                    .filter(tableDataFileName -> tableDataFileName.matches(createPrefixTableName + "_[0-9]{2}"))
                    .collect(Collectors.toList());
            for (String tableDataFileName :
                    tableDataFileList) {
                Logger.debug("Create table: " + tableDataFileName);
                DBConnector.createTable(new File(tableDataFileName));
                numCreateTable++;
            }
        } else {
            Logger.error("Not Found table data file.");
        }
        return numCreateTable;
    }
}
