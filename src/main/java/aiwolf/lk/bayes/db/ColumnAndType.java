package aiwolf.lk.bayes.db;

/**
 * 列名と型名のタプルを保持するクラス
 */
public class ColumnAndType {
    private String columnName;
    private String type;

    ColumnAndType(String columnName, String type) {
        this.columnName = columnName;
        this.type = type;
    }

    @Override
    public String toString() {
        return columnName + " " + type;
    }
}
