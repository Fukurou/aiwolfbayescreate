package aiwolf.lk.bayes.db;

import aiwolf.lk.bayes.util.PropertyUtil;
import fukurou.logger.LogLevel;
import fukurou.logger.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * データベースとの接続を担うクラス
 */
public class DBConnector {

    private static String connectionURL;

    static {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            Logger.fatal("SQL Server Library is not Found");
        }

        String sqlServerDomainName = PropertyUtil.getProperty("sqlServerDomainName", "localhost", LogLevel.TRACE);
        String sqlServerPortNumber = PropertyUtil.getProperty("sqlServerPortNumber", "1433");
        String sqlServerUserName = PropertyUtil.getProperty("sqlServerUserName", "SA");
        String sqlServerUserPassword = PropertyUtil.getProperty("sqlServerUserPassword");
        if (sqlServerUserPassword.equals("")) {
            Logger.fatal("Need set password for to connect SQL Server.");
        }
        String SqlServerDatabaseName = PropertyUtil.getProperty("SqlServerDataBaseName", "BayesCreate", LogLevel.TRACE);
        connectionURL = "jdbc:sqlserver://" + sqlServerDomainName + ":" + sqlServerPortNumber + ";databaseName=" + SqlServerDatabaseName + ";user=" + sqlServerUserName + ";password=" + sqlServerUserPassword;
    }

    /**
     * テーブルを作成する
     *
     * データベース上に同名のテーブル名が存在する場合、削除する
     * その後、再生成される
     *
     * @param tableDataFile 作成するテーブルの情報が載っているファイル
     */
    public static void createTable(File tableDataFile) {
        dropTable(tableDataFile.getName());

        List<ColumnAndType> columnList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(tableDataFile))) {
            String readLine;
            while ((readLine = bufferedReader.readLine()) != null) {
                String[] word = readLine.split(",");
                ColumnAndType columnAndType = new ColumnAndType(word[0], word[1]);
                columnList.add(columnAndType);
            }
        } catch (IOException e) {
            Logger.fatal("Failed read of table data");
        }

        List<String> columnListString = new ArrayList<>();
        columnList.forEach(columnAndType -> columnListString.add(columnAndType.toString()));

        String sql = "CREATE TABLE " + tableDataFile.getName() + "(" + String.join(",", columnListString) + ")";
        try (Connection connection = DriverManager.getConnection(connectionURL); Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            Logger.error("Failed create a table");
        }
    }

    /**
     * DBからテーブルを削除する
     * @param tableDataFileName 削除するテーブル名
     */
    private static void dropTable(String tableDataFileName) {
        if (isTableExist(tableDataFileName)) {
            try (Connection connection = DriverManager.getConnection(connectionURL); Statement statement = connection.createStatement()) {
                statement.executeUpdate("DROP TABLE " + tableDataFileName);
            } catch (SQLException e) {
                Logger.error("Occer exception when drop table from DB");
            }
        }
    }

    /**
     * DBにテーブルが存在するかを確認する
     * @param tableDataFileName 確認するテーブル名
     * @return テーブルが存在することを確認した場合にtrueを返す
     */
    private static boolean isTableExist(String tableDataFileName) {
        try (Connection connection = DriverManager.getConnection(connectionURL)) {
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            ResultSet resultSet = databaseMetaData.getTables(null,null,null, new String[]{"TABLE"});
            while(resultSet.next()) {
                if (tableDataFileName.equals(resultSet.getString("TABLE_NAME"))) {
                    return true;
                }
            }
        } catch (SQLException e) {
            Logger.error("Occur exception when check exist table to data base.", e);
        }
        return false;
    }

    /**
     * 列名のリストを返す
     * @param tableName 調べるテーブル名
     * @return 列名のリスト
     */
    public static List<String> columnNameOfTable(String tableName) {
        List<String> columnList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(connectionURL); Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = " + tableName);
            while (resultSet.next()) {
                columnList.add(resultSet.getString("COLUMN_NAME"));
            }
        } catch (SQLException e) {
            Logger.error("Occered SQLException", e);
        }
        return columnList;
    }

    /**
     * レコードをDBへINSERTする
     * @param tableName 挿入するテーブル名
     * @param recordMap レコード
     */
    public static void executeUpdate(String tableName, Map<String, String> recordMap) {
        try (Connection connection = DriverManager.getConnection(connectionURL); Statement statement = connection.createStatement()) {
            List<String> fieldList = new ArrayList<>();
            List<String> valueList = new ArrayList<>();

            // 要素名と用措置をわける
            recordMap.forEach((key, value) -> {
                fieldList.add(key);
                valueList.add(convert(value));  // 文字列の場合シングルクォートで囲む
            });

            String fieldString = String.join(",", fieldList);
            String valueString = String.join(",", valueList);
            String sql = String.format("INSERT INTO %s (%s) VALUES(%s);", tableName, fieldString, valueString);
            Logger.trace("SQL: " + sql);
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            Logger.error("Occerd SQLException", e);
        }
    }

    private static String convert(String string) {
        if (string == null || string.equals("null")) {
            return "NON";
        }
        String regex = "^[0-9]+";
        if (string.matches(regex)) {
            return string;  // 数値
        } else {
            return "'" + string + "'"; // 文字列
        }
    }
}
