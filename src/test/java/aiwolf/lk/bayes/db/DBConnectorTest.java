package aiwolf.lk.bayes.db;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class DBConnectorTest {

    @Test
    public void createTable() {
        // resources/tableData/VILLAGER_5_5.csvを使ってDBを作成しエラーが出ないことを確認する。
        DBConnector.createTable(new File("resources/tableData/VILLAGER_5_5.csv"));
    }
}