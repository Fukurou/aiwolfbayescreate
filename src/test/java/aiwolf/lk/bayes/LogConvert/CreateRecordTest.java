package aiwolf.lk.bayes.LogConvert;

import org.aiwolf.common.data.Agent;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.Assert.*;

public class CreateRecordTest {

    @Test
    public void testExtractValue() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        File testLogFile = new File("resources/serverLog/cedec2018_5/0.txt");
        LogPick logPick = new LogPick(testLogFile);

        String tableName = "nonTable";  // extractValueのテストには使われない
        CreateRecord createRecord = new CreateRecord.Builder()
                .logPick(logPick)
                .tableName(tableName)
                .submit(Agent.getAgent(1))
                .target(Agent.getAgent(2))
                .day(1).build();

        String expected = "1";  // 期待値
        String columnName = "status_divinedResultAll_AgentT";   // テストする列名
        Method method = CreateRecord.class.getDeclaredMethod("extractValue", String.class);
        method.setAccessible(true);
        String actual = (String)method.invoke(createRecord, columnName);

        assertEquals(expected, actual);
    }
}