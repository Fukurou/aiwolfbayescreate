package aiwolf.lk.bayes.LogConvert;

import aiwolf.lk.bayes.util.PropertyUtil;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

import static org.junit.Assert.*;

public class LogConverterTest {

    @Test
    public void testConvert() {
    }

    @Test
    public void testLogFileList() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        LogConverter logConverter = new LogConverter();
        Method method = LogConverter.class.getDeclaredMethod("logFileList");
        int expected = Objects.requireNonNull(new File(PropertyUtil.getProperty("serverLogFilePath")).list()).length;

        method.setAccessible(true);

        String[] array = (String[])method.invoke(logConverter);

        assertEquals(expected, array.length);
    }
}